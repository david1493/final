#########################################################
################# CREDENTIALS ###########################

subscription_id = "49d2709f-3abb-47e9-abb6-ce0c26be47f2"
client_id       = "d1fef633-a370-4822-bc5c-0dc2ba41d41c"
client_secret   = "62381b0e-f22b-45fd-8525-4ec06cc32867"
tenant_id       = "edfa0b8b-916d-4bb7-aab9-4db7fb09786a"
ssh_keys_data   = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDANXKDaDDNvsUxbCArz0rQjqRgrobUeikTKkCoXbBCL7qfOxaMtWt5Mq92MQRu2JbYAZvbxIjCX3iUqBBKV6m7ZnecBZxK9SodGlcI2cUy7pD6OyYDKWdsy6hWktBCE6hosR+8sAjGr2/NQ3Z0UH55KLyL8hx5Bl6u41I7Z8AQ9LxNG0aow1MXFCTelF7GZ3J9I9STtQl9qus9Rr4G764oKVDGl/MtKd931IKxskNo4giVU2cyGcDTLhoG6esS0dU+j1eb3MDdJP+uSiA7/2sXdBwyha2cqIXSp1kBSVqEM2e70MYphBXWtaSQTqnTuXX12R13/e2EXsLm34wXTIlF stage@CentOs.formation"
ssh_keys_path   = "/home/stage/.ssh/authorized_keys"

########################################################
#################### GENERAL ###########################

location      = "eastus"
boolean_true  = "true"
boolean_false = "false"

########################################################
################ RESOURCE GROUP ########################

resource_group_name = "myResourceGroup"

#########################################################
############### VIRTUAL NETWORK #########################

virtual_network_name          = "myVnet"
virtual_network_address_space = "10.0.0.0/16"

#########################################################
################ PUBLIC IP ##############################

myterraformpublicip_name  = "myPublicIP"
myterraformpublicip2_name = "myPublicIP2"
address_allocation        = "dynamic"

#########################################################
################## SUBNET ###############################

myterraformsubnet_name = "mySubnet"
myterraformsubnet_address_prefix = "10.0.2.0/24"

#########################################################
############## NETWORK SECURITY GROUP ###################

myterraformnsg_name = "myNetworkSecurityGroup"
myterraformnsg2_name = "myNetworkSecurityGroup2"

#########################################################
############### NETWORK INTERFACE #######################

myterraformnic_name = "myNIC"
myterraformnic2_name = "myNIC2"
myterraformnic_ip_configuration = "myNicConfiguration"
myterraformnic2_ip_configuration = "myNic2Configuration"

#########################################################
############## STORAGE ACCOUNT ##########################

storage_account_replication_type = "LRS"
storage_account_tiers = "Standard"

#########################################################
############### VIRTUAL MACHINE #########################

myterraformfirstvm_name = "devops-app-01"
myterraformsecondvm_name = "myVM2"
virtual_machine_vm_size = "Standard_DS1_v2"

#########################################################
##################### TAGS ##############################

environment = "Terraform Demo"

